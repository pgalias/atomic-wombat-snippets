# Connect to a DocumentDB cluster with MongoEngine

## Prerequisites

You need to install MongoEngine (doc [here](https://mongoengine-odm.readthedocs.io/)). You also need to define an environment variable called `STAGE` with the value `dev`, `test` or `prod`. 

For dev, you can run a MongoDB instance with Docker or docker-compose locally.

For test, you also need to install MongoMock (doc [here](https://pypi.org/project/mongomock/)) to mock MongoDB.

For prod, you need to create an AWS DocumentDB cluster (doc [here](https://docs.aws.amazon.com/documentdb/latest/developerguide/getting-started.html))

## Snippet

```py
from mongoengine import connect

def connect_to_documentDB():
    stage = os.environ["STAGE"]
    if stage == "dev":
        conn = connect(host="mongodb://root:example@localhost:27017")
    elif stage == "test":
        # Import mongomock here to avoid importing it in production
        import mongomock
        conn = connect(
            "mongoenginetest",
            host="mongodb://localhost",
            mongo_client_class=mongomock.MongoClient,
        )
    elif stage == "prod":
        cluster_uri = get_cluster_uri()  # Provided by AWS in the cluster details
        conn = connect(host=cluster_uri)
    else:
        raise ValueError(f"Unknown stage: {stage}")

    if conn.server_info():
        print("Successfully connected to DocumentDB")
```