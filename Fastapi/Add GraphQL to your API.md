# Add GraphQL to your API

## Prerequisites

You need to install Strawberry library in your project (doc [here](https://strawberry.rocks/)).

## Snippet

```py
import typing

from fastapi import FastAPI
import strawberry
from strawberry.fastapi import GraphQLRouter


@strawberry.type
class Book:
    title: str
    author: str


def get_books():
    return [
        Book(
            title="The Great Gatsby",
            author="F. Scott Fitzgerald",
        ),
        Book(
            title="I, Robot",
            author="Isaac Asimov",
        ),
    ]


@strawberry.type
class Query:
    books: typing.List[Book] = strawberry.field(resolver=get_books)


schema = strawberry.Schema(query=Query)
graphql_app = GraphQLRouter(schema)

app = FastAPI()
app.include_router(graphql_router, prefix="/graphql")
```

# Tips

You can access a GraphQL playground when running your Fastapi app on `http://localhost:8000/graphql`.