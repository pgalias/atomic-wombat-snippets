# Use useSWR to fetch data in client components

## Prerequisites

You need to install `swr`. Documentation about useSWR can be found [here](https://swr.vercel.app/).

## Make a simple GET request to an endpoint

```tsx
import useSWR from "swr";

const fetcher = (url: string) => fetch(url).then((res) => res.json());

const { data, error, isLoading } = useSWR(
  "/api/users/1234/profile",
  fetcher
);
```

## Make GET request to a protected endpoint (with jwtToken)

```tsx
import useSWR from "swr";

const fetcherWithAuth = ([url, token]) =>
  fetch(url, {
    headers: {
      Authorization: token,
    },
  }).then((res) => res.json());


  const { data, error, isLoading } = useSWR(
    [
      "/api/users/1234/profile",
      jwtToken,
    ],
    fetcherWithAuth
  );
```